using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartManager : MonoBehaviour {

    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite halfFullHeart;
    public Sprite emptyHeart;
    public FloatValue heartContainers; // will take the value from the script HeartContainer
    public FloatValue playerCurrentHealth; // will take the value from the script PlayerHealth

	// Use this for initialization
	void Start () {
        InitHearts(); // allow to start with the number of hearts we specified in InitHearts method
	}

    public void InitHearts()
    {
        for (int i = 0; i < heartContainers.initialValue; i ++)
        {
            hearts[i].gameObject.SetActive(true); // player as all is heart, so we can see him (is is active)
            hearts[i].sprite = fullHeart; // has the player has all his hearts, we chose to display full heart 
        }
    }

    // allow to update hearts in fucntion of different condition all along the game
    public void UpdateHearts() 
    {
        float tempHealth = playerCurrentHealth.RuntimeValue / 2; // we divid by 2 to because we condiser 1 point a life as 1 half heart
        for (int i = 0; i < heartContainers.initialValue; i ++) // we loop over heart container to compare player current health value with hearth inside the heart container. One i is equal to 1 heart
        {
            if(i <= tempHealth-1) // we put -1 for tempHealth because i start at 0. For example, if playerCurrentHealth is equal to 8. We divid by 2 (that give us the tempHealth) (because 1 point of life is currenlty 1 half heart). We go to the hrat container (each heart i equal to 1). For a loop inside a heart container of 4 full heart : first iteration 0 <= tempHealth who is equal to 3 (so full heart), next ietration 1 <= 3 (full heart), 2<= 3 (full heat), 3<=3 (full heart). we stop here because we said in the loop tempHealth -1 (4 iteration if we ocunt the 0)
            {
                //Full Heart
                hearts[i].sprite = fullHeart; // we display the full heart image (that we put in the inspector)
            }else if( i >= tempHealth) 
            {
                //empty heart
                hearts[i].sprite = emptyHeart; 
            }else{
                //half full heart
                hearts[i].sprite = halfFullHeart;
            }
        }

    }

}

// TO UNDERSTAND empty heart and half full heart 
// the player recieve one attack. So, the currentHealth pass 8 to 7. 
// the temp health value become 3.5
// for the condition empty heart : 0  (nothing), 1 (nothing), 2 (nothing), 3 (nothing) - > nothing append because the last iteration (3) is not > ou = to 3.5 (temp health value)
// fot the condition full heart : 0  (nothing), 1  (nothing), 2  (nothing), 3 (nothing)  -> as nothing append for the last condition empty heart : we take the condition hal full heart and retrieve 1 half heart
// another attack, current health become 6 and tampHealth pass to 3 :
// for the condition empty heart : 0 (nothing), 1 (nothing), 2 (nothing), 3 (nothing) ->  here 3 is > or = to temp health. So the condition is activate (empty heart is displayed)
// fot the condition full heart : 0 : 1 (nothing), 1 : 2 (nothing), 2 : 3 (nothing), 3 : 4 (nothing), 4 : 0 (nothing)
// etc...

