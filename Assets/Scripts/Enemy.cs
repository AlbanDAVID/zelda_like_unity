using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// create a kind of list with the different state we want for an enemy
public enum EnemyState{
    idle,
    walk,
    attack,
    stagger
}

public class Enemy : MonoBehaviour
{
    public EnemyState currentState; //give the name to enmu list we created above. EnemyState will allow to have a scolling menu named currentState in the enemy inspector
    public FloatValue maxHealth; // FloatValue refer to the script we created inside the folder ScriptableObjects
    public float health;
    public string enemyName;
    public int baseAttack;
    public float moveSpeed;



    private void Awake() // when we push play (it's like a void), the enemy will be affected by this function. In this case, we will asign the max health of the enemy.
    {
        health = maxHealth.initialValue; // retrieve the initial value in the Two Hit Enemy Health script (from Float Value script).
    }

    private void Start()
    {
        health = maxHealth.initialValue; // retrieve the initialValue from the scriptable objects "FloatValue"
    }

    // function to withdraw heatlh 
    private void TakeDamage(float damage)
    {
        health -= damage; // 
        if(health <= 0)
        {
            this.gameObject.SetActive(false); // if the health <= : the enemy disappear
        }
    }

    // this public method allow to use KnockCo in another script and also TakeDamage
    public void Knock(Rigidbody2D myRigidbody, float knockTime, float damage)
    {
        StartCoroutine(KnockCo(myRigidbody, knockTime));
        TakeDamage(damage);
    }

   // when we knockback an enemy we want that is stop when a certain distance is reached
   // it's why we created the knockTime (how many time the enemy will be knocked back)
   private IEnumerator KnockCo(Rigidbody2D myRigidbody, float knockTime)
   {
    if(myRigidbody != null)
    {
        yield return new WaitForSeconds(knockTime); // how long time the knocking back will last
        myRigidbody.velocity = Vector2.zero; // to stop the enemy ?
        currentState = EnemyState.idle; // quit stagger state and return to idle state
        myRigidbody.velocity = Vector2.zero;
    }
   }
}


