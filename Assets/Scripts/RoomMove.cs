using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomMove : MonoBehaviour
{

    public Vector2 cameraChange;
    public Vector3 playerChange;
    private CameraMovement cam;


    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.GetComponent<CameraMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // lorsque la caméra change de room, l'axe des Y continue d'incrémenter par rapport
    // à l'anciennce room 
    // par ex : min position de y en room 1 = -17 
    // sachant, que la map de la prochaine room fait à peu près 29 (pixel ?) de hauteur (comme la précédente) :
    // -1.8 + 29 = 27
    // 27 constitue le nouveau repaire du min de l'axe y pour la nouvelle room (en fonction de la précédente)
    // la logique est la même pour le max sur l'axe y : y max = 16 dans la room 1.
    // 16 + 29 = 45. Ainsi, le nouveau max en y pour la position de la caméra dans la room 2 est de 45 
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.CompareTag("Player") && !other.isTrigger) // ?? if the object is tagged player and if the player's collider is not the trigger one ??
        {
            cam.minPosition += cameraChange;
            cam.maxPosition += cameraChange;
            other.transform.position += playerChange;
        }
    }
}
