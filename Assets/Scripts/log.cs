using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script will herite from the script named "Enemy" for general things for Enemies
// this script "log" will add specifcs variables and methods for you enemy "log" 
public class log : Enemy
{
    private Rigidbody2D myRigidbody;
    public Transform target; // allow to hace the space location of an object
    public float chaseRadius; // the area inside the log will chase the player
    public float attackRadius; // the area inside the log will attack the player
    public Transform homePosition; // if the player is not inside the chase raidus, the log return into sleep position
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        currentState = EnemyState.idle;
        myRigidbody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        target = GameObject.FindWithTag("Player").transform; // allow to get the space location of the game object "Player"
    }

    // Update is called once per frame
    void FixedUpdate() // Update runs once per frame. FixedUpdate can run once, zero, or several times per frame, depending on how many physics frames per second are set in the time settings, and how fast/slow the framerate is.
    {
        CheckDistance();
    }

    // check if the difference betwenn log position (transform.position) and the player (target.position) is less or equal 
    // to the chase radius (the area where the log will chase the player). Furthermore, verifiy if the chase radius > attackRadius.
    // if it's the case, the log will purchase the player :
    // transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime); 
    // the log position (tranform.position) will move towards the target with the speed we defined in the moveSpeed variable (Enemy script)
    // If the chase radius become less than the chase radius, the log will be in attack mode
    // if the player quit the chase radius, the log will come back to homePosition (sleep) and stop to follow the player
    // transform.position is equal to the log position
    // target.position is equal to the player position
    void CheckDistance()
    {
        if(Vector3.Distance(target.position, transform.position ) <= chaseRadius && Vector3.Distance(target.position, transform.position) > attackRadius) // check if we are in the chase area
        {
            if(currentState == EnemyState.idle || currentState == EnemyState.walk && currentState != EnemyState.stagger) // we want that the enemy follow the player only if he is walking or idle but not knocked out (stagger)
            {
                Vector3 temp = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime); // allow to assign a speed to the log and move toward the player. MoveToward is a function which allow follow an target. In this, case, the target that we defined above is the player 

                changeAnim(temp - transform.position); // to get the actual amount of the movement that happening
                myRigidbody.MovePosition(temp); // ?? allow to move the log's rigid body ??
                ChangeState(EnemyState.walk);
                anim.SetBool("wakeUp", true); // allow to start the Walking animiation in the animator
            }
            }else if (Vector3.Distance(target.position, transform.position ) > chaseRadius){ // to avoid log's sleeping in the chase area
                anim.SetBool("wakeUp", false);
            }
        }

        // a vector of animator. equal 0 for now. we will fill the value to determine wich is the position 
        // for example if we assign right to this vectir, he will start the right animation we put in the animator
        private void SetAnimFloat(Vector2 setVector){
            anim.SetFloat("moveX", setVector.x);
            anim.SetFloat("moveY", setVector.y);
        }
        // we want to change the MovePosition (temp variable) of the log
        // if X or Y is greater, I will decide to turn left or right
        private void changeAnim(Vector2 direction){
            // check if we are in x position (horizontal position)
            if(Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
            {
                // check if we are the roght horizontal
                if(direction.x > 0){
                    // start the animation we put in right in the animator 
                    SetAnimFloat(Vector2.right);
                // check if we are the left horizontal
                }else if (direction.x <0)
                {
                    SetAnimFloat(Vector2.left);
                }
            }else if(Mathf.Abs(direction.x) < Mathf.Abs(direction.y)){
                if(direction.y > 0)
            {
                SetAnimFloat(Vector2.up);
            }else if (direction.y <0)
            {
                SetAnimFloat(Vector2.down);
            }
        }    
    }
    

    private void ChangeState(EnemyState newState){
        if(currentState != newState)
        {
            currentState = newState;
        }
    }
}
