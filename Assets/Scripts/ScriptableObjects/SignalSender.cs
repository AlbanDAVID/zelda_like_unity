using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SignalSender : ScriptableObject { // icalled SignalSender and not Signal because Signal is already took by unity. If we wllaed signal we will not be able to create a Signal script

    public List<SignalListener> listeners = new List<SignalListener>(); // allow to have a Listerner List in the inspector menu of of Signal Sender script type 

    public void Raise() // Raise will allow to raise a signal to send. Will loop inside the list of listener and for each of them, going to raise the method created
    {
        for (int i = listeners.Count - 1; i >= 0; i --) // if a signal is sended, we remove the listerner from the list (avoid to create another sending alreardy send)
        {
            listeners[i].OnSignalRaised(); // this method is inside SignalListener script
        }
    }
    // allow to add a listerner in the list "listerner" we created above (this method will be called in the signal listerner)
    public void RegisterListener(SignalListener listener)
    {
        listeners.Add(listener);
    }

    // allow to remove a listener (this method will be called in the signal listerner)
    public void DeRegisterListener(SignalListener listener)
    {
        listeners.Remove(listener);
    }

}