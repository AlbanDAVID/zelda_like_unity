using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// allow to create independant script
// now, if we click right to script folder, we can create a float value script

[CreateAssetMenu]
public class FloatValue : ScriptableObject, ISerializationCallbackReceiver // allow to serialize the new FLoatValue ? will allow to cache value .. ? for the float value 
{
    public float initialValue; // a variable assigned to each enemy 

    [HideInInspector] // not show is the inspector
    public float RuntimeValue;

    public void OnAfterDeserialize(){
        RuntimeValue = initialValue;
    }
    public void OnBeforeSerialize(){}

}
