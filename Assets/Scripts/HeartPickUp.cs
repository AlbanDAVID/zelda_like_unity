using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartPickUp : MonoBehaviour
{

    public FloatValue playerCurrentHealth;
    public SignalSender playerHealthSignal;
    public FloatValue heartContainers;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if(this.gameObject.CompareTag("pickUp") && other.gameObject.CompareTag("Player") && !other.isTrigger) // Without !other.isTrigger, the method we put happend 2 times ...
        {   
            UpdateHeartsPickUp();
            playerHealthSignal.Raise();
            this.gameObject.SetActive(false);
        }
    }

    public void UpdateHeartsPickUp()
    {
        float tempHeartContainers = heartContainers.RuntimeValue * 2; 
        if ((tempHeartContainers - 1) == playerCurrentHealth.RuntimeValue)
        {
            playerCurrentHealth.RuntimeValue += 1;
            
        }
        else if(playerCurrentHealth.RuntimeValue <= tempHeartContainers - 2)
        {
            playerCurrentHealth.RuntimeValue += 2;
        }

    }

}

