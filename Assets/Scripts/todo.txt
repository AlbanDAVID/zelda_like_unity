To do : 
- pk pas de coup d'épée en déplacement diagonale gauche
- pk quand je suis stagger en horzontal ou vertical parfait, la vitesse de frappe et plus importante? 
- fix les changements de map incertains 
- comprendre tout ce qu'il y après [HideInInspector] dans le object script FloatValue


CHALLENGE :
- faire transition avec des pièces qui n'ont pas les mêmes dimensions
- créer des map otimisées : càd : pas grid dans le le game et avec Tiled par ex ?

---------
NOTE :
---------

1) 
The player object has 2 collider :
- a box collider for collid 
- a capusle collider whom is a trigger (mainly use to use an attack when we banged to a mob)

-------
2) 
- SCRIPTABLE OBJECTS : 
- allows to create a script which can be assign to an object :
- for example, we have several enemies. We want that this enemy start with different maxHealth.
- we cerate a script object and create a script (for example, FloatValue)
- after we create a c# script of FLoatObject type (for example TwoHitEnemyHealth)
- when we open this object, we can assign an initial value. For example 4
- after for each enemy, we assign the script object TwoHitEnemyHealth :  this script TwoHotEnemy has a FloatValue type ! (refer to the FoatValue script we created)
- after that, when we go to the script in the inscpetor of the chosen enemy, we go to Max maxHealth and when we push start, the max heatl will be automacaliy to 4 (because we decide that above)
- of course, we can cretate several script from FLoatType and assign different value of an enemy... For example, some enemies will start with 4 health and other with 10..

- Scriptable Object is a data container (not dependant of mono behavior)
- it's a scene independant : any game object from any scene can access this script object
-------
3) 
OBSERVER PATTERN SENDING SIGNAL (a scripatable objects) :
- Object in a game that watches things happen 
- and use this information to tell other objects what to do
- For example, for the health UI :
- the hearts does not needs to know informations about the player 
- In fact, the player just needs to send to the hearts a signal 
- to say : ths is how much health I have now
- and heatts display the corresponding numer of hearts
- don't need to kow about the player information, so no rigid connections
- the player just send signals to the hearts UI
- when an object is suscribed to a signal, he will recieve the signal and do something 

- for example, in the hearth holder. we put the the signal listener script. And now, we can add a signal that we want 
- this heart holer listen an event. In this case, we chose the event  "update heart" method from the heart containers.
- Thus, now hearth will be up to date because the heart holder lister what to do (add or remove hearth) in terms on what append on the unpdate heart metgod from hearth container script

------
4) UNDERSTANDING SCRIPT OBJECT FOR HEARTH UI 

- if the player movement script we have the method Knock
- so when the player is knocked : a signal will be send : playerHealthSignal.Raise(); 
- where this signal is sended ? in the HealthSignal who is a sciprtable object (Signal Sender script)
- the signal sender script will send a signal to the listener script
- where is the listner script ? he is in the Heart Holder (signal listener script)
- the signal listener applied the method update heart from the script HealthManager
- Thus, the number of heart can be updated.