using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraMovement : MonoBehaviour
{
    // create objects that we want to follow with camera
    public Transform target;
    public float smoothing; // to adjust how the camera move towards the target in other word : in which intensity the camera will catch up the player 
    public Vector2 maxPosition;
    public Vector2 minPosition;


    // LaterUpdate allow to move camera before player move
    void LateUpdate()
    {
        if(transform.position != target.position) // transform.position is the position of the camera (we don't have to define the variable because we put the script in the camera movement, so we take automatacally the transform of the camera movement). target position is the position of the player. we defined above a player, and in the inscpetcor, we put the player for this target
        {
            Vector3 targetPosition = new Vector3(target.position.x,
                                                 target.position.y,
                                                 transform.position.z); // because we want that z position be the z position of the camera and not the z position of the player (-10 in order to see the scene from above). Its why we created this nex vector 3 targetPosition : in order that z positon always stay at -10 when the camera postion will change. Indeed, the camera posotion will follow the target on x et y (to always be the same w and that the player). However, the player z posotion is 0 ! and we need that the Z position be -10. SO, we putted the transform.position in z. Thus, the camera will follow is own value -10 (this value does not change when the player move because it's the z position of the camera, so z will always stays at -10)

            // camera boudaries in x position, we modify the targetPosition
            // Mathf.Clamp :  targetPosition.x : The floating point value to restrict inside the range defined by the minimum and maximum values.
            // minPosition.x : The minimum floating point value to compare against.
            // maxPosition.x : The maximum floating point value to compare against.
            targetPosition.x = Mathf.Clamp(targetPosition.x,
                                           minPosition.x,
                                           maxPosition.x);
            // camera boudaries in y posotion, we modify the targetPosition
            targetPosition.y = Mathf.Clamp(targetPosition.y,
                                           minPosition.y,
                                           maxPosition.y);

            // function Lerp (linear interpolation)
            // will handle the transform.position of the camera 
            // a linear interpolation that allow to follow the target position. 
            // Will deplace the camera (transform.position) linearly to the disired destination (target.Position, the player). 
            //This move will be effectued every second we specified in smoothing. 
            //In antoher word, camzera position will follow the player linearly every seconds we specify (smoothing)
            // Another explanation : 
            // transform.position is the sart value and this start value have to reach the end value (targetPostion)
            // simply, transform.position (the camera) will take the position of the target (allow to follow the target)
            // smoothing at 0 : the interpolation will be transform.position (transform.position stay transform.posotion and the camera don't follow the target)
            // smoothing at 1 : the interpolation will be targetPosition : the position of the camera become the position of the target Position 
            // Thus, she stricly follow the target
            // if smoothing is for example 0.1 (there will be a delay of 100 ms of interpelation) :
            // we will wait 100 ms for that the transform.posotion become the same position as targetPosition 
            // just above (targetPosition.x and targetPosition.y) we defined some boundaries
            // Thus, the targetPosition is limited by x and y. So the transformPosition will never be above this limit. It's why the camera 
            // stopped in your 25 * 25 scene. 
            // when the bounadaries are reached : no interpolation, so no camera movement.
            // it's like the smoothing were at 0 (no interpolation, transform.position stay transform position)
            transform.position = Vector3.Lerp(transform.position,
                                              targetPosition, smoothing); 
        }
    }
}

