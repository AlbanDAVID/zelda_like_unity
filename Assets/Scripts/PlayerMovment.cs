using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    walk,
    attack,
    interact,
    stagger,
    idle
}

public class PlayerMovment : MonoBehaviour
{
    public PlayerState currentState;
    public float speed;
    private Rigidbody2D myRigidbody;
    private Vector3 change;
    private Animator animator;
    public FloatValue currentHealth; 
    public SignalSender playerHealthSignal;

    // Start is called before the first frame update
    void Start()
    {
        currentState = PlayerState.walk;
        animator = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
        animator.SetFloat("moveX", 0);
        animator.SetFloat("moveY", -1);
    }

    // Update is called once per frame
    void Update()
    {
        change = Vector3.zero;
        change.x = Input.GetAxisRaw("Horizontal");
        change.y = Input.GetAxisRaw("Vertical");
        if(Input.GetButtonDown("attack") && currentState != PlayerState.attack
        && currentState != PlayerState.stagger) // we can attack if we are already attacking or if we are stagged
        {
            StartCoroutine(AttackCo());
        }
        else if (currentState == PlayerState.walk || currentState == PlayerState.idle)
        {
            UpdateAnimationAndMove();
        }
    }

    private IEnumerator AttackCo() // co routine for attack
    {
        animator.SetBool("attacking", true);
        currentState = PlayerState.attack;
        yield return null;
        animator.SetBool("attacking", false);
        yield return new WaitForSeconds(.3f);
        currentState = PlayerState.walk;
    }

    void UpdateAnimationAndMove()
    {
        if (change != Vector3.zero)
        {
            MoveCharacter();
            animator.SetFloat("moveX", change.x);
            animator.SetFloat("moveY", change.y);
            animator.SetBool("moving", true);
        }else{
            animator.SetBool("moving", false);
        }
    }

    void MoveCharacter()
    {
        change.Normalize(); // avoid too much speed in diagonal move
        myRigidbody.MovePosition(
            transform.position + change * speed * Time.deltaTime
        );
    }

    // this public function allow to use KnockCo in another script
    public void Knock(float knockTime, float damage)
    {
        currentHealth.RuntimeValue -= damage; // the current health is a FloatValue scriptable object. This is the health xhich the player start. WHen the Knock method is called, we retrieve the number of life fr
        playerHealthSignal.Raise(); // allow to send a signal to signal. In this case, we will put the script object health signal. A signal will appear to the listener when the player un knocked out
        if(currentHealth.RuntimeValue> 0)
        {
        
        StartCoroutine(KnockCo(knockTime));
        }else{
            this.gameObject.SetActive(false);
        }
    }

   // when enemy knocked back the Player, we want that the player stop to be back when a certain distance is reached
   // it's why we created the knockTime (how many time the Player will be knocked back)
   private IEnumerator KnockCo(float knockTime)
   {
        if(myRigidbody != null)
        {
            yield return new WaitForSeconds(knockTime); // how long time the knocking back will last
            myRigidbody.velocity = Vector2.zero; // to stop the enemy ?
            currentState = PlayerState.idle; // quit stagger state and return to idle state
            myRigidbody.velocity = Vector2.zero; 
        }
    }
}


