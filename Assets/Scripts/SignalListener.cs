using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events; // because we need events from unity system (to recieve the signal from signal sender ?)

public class SignalListener : MonoBehaviour { // herite from MonoBehaviour because will take effect int the actual game object

    public SignalSender signal; // the signal who will be listened (it's a SignalSender script object type (from the script SignalSender))
    public UnityEvent signalEvent; // ??

    public void OnSignalRaised() // when the signal is raised (so, when the signal is recieved and listened ?), will do something
    {
        signalEvent.Invoke(); // call the event that we putted in the signal variable. This method come from UnityEngine.Events; and Invoke all registered callbacks (runtime and persistent).
    }
    
    private void OnEnable()
    {
        signal.RegisterListener(this); // allow to register this listerner in the list listener
    }
    private void OnDisable()
    {
        signal.DeRegisterListener(this); // allow to deregister this listerner in the list listener once the messahe had been listened
    }
}