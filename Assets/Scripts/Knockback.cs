using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Knockback : MonoBehaviour
{


   public float thrust; // how much force the player will give when he attacks
   public float knockTime;
   public float damage; // the damage that the enemy or player will do. (for the Player, the damage variable is inside the inspector of different hit boxes )
   

   // check if an enemy is in the player's trigger area 
   // and if it's the case :  affect the rigidbody system  
   private void OnTriggerEnter2D(Collider2D other)
   {
        // allow to break pot (Smash() function in pot.cs)
        if(other.gameObject.CompareTag("breakable") && this.gameObject.CompareTag("Player"))
        {
            other.GetComponent<pot>().Smash();
        }
       if(other.gameObject.CompareTag("enemy") || other.gameObject.CompareTag("Player")) // check if the object is tagged "enemy" or "Player" (because the player can be knocked back by an enemy)
       {
           Rigidbody2D hit = other.GetComponent<Rigidbody2D>(); // we get the rigibody of the game object that we want knocked back (Palyer or enemy)
           if(hit != null) // check if the object is a rigid body
           {
               // if the enemy attacks Player or Player attacks enemy
               Vector2 difference = hit.transform.position - transform.position; // did not understand what is transform.position here
               difference = difference.normalized * thrust; // allow to knock back the enemy (give a distance between the player and the enemy)
               hit.AddForce(difference, ForceMode2D.Impulse); // the impulse add an instance to the rigidbody2D. Allow knock back nemy when the player attacks him
               // only if the player attack the enemy :
               // we change the state of the enemy and start the coroutine Knock
               if(other.gameObject.CompareTag("enemy") && other.isTrigger)// if the game object in tagged enemy and is it's collider is a trigger (because enely has 2 collider). One is a trigger and it's this one we want
               {
                    hit.GetComponent<Enemy>().currentState = EnemyState.stagger;
                    other.GetComponent<Enemy>().Knock(hit, knockTime, damage); // Call the Knock function (in Enemy script) and allow to apply a knock time and a damage (withdraw health of the enemy)
               }
               // only if an enemy attcks the player :
               //  we change the state of the Player and start the coroutine Knock
               if(other.gameObject.CompareTag("Player"))
               {     
                    if(other.GetComponent<PlayerMovment>().currentState != PlayerState.stagger)
                    {
                    hit.GetComponent<PlayerMovment>().currentState = PlayerState.stagger;
                    other.GetComponent<PlayerMovment>().Knock(knockTime, damage); // go the PlayerMovment script the start the knock method. The damage value correspond to the damage value put inside the enely who is attacking
                    }
               }
               
      
           }
       }
   }
}